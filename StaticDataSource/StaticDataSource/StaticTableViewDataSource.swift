//
//  StaticTableViewDataSource.swift
//  StaticDataSource
//
//  Created by Maciej Jastrzebski on 22/04/16.
//  Copyright © 2016 Objectivity Ltd. All rights reserved.
//

import UIKit

class StaticTableViewDataSource: NSObject, UITableViewDataSource {
    let dataSource: StaticDataSource

    init(dataSource: StaticDataSource) {
        self.dataSource = dataSource
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return dataSource.sections.count
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.sections[section].itemCount
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let section = dataSource.sections[indexPath.section]
        let reuseIdentifier = section.reuseIdentifierForItem(indexPath.row)

        let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier, forIndexPath: indexPath)
        section.bindCell(cell, withItem: indexPath.row)
        return cell
    }

    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return dataSource.sections[section].header
    }

    func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return dataSource.sections[section].footer
    }

    func keyForIndexPath(indexPath: NSIndexPath) -> String? {
        return dataSource.keyForIndexPath(indexPath)
    }
}

extension StaticTableViewDataSource {
    convenience init(sections: [StaticSectionType]) {
        self.init(dataSource: StaticDataSource(sections: sections))
    }

    convenience init(sections: StaticSectionType...) {
        self.init(dataSource: StaticDataSource(sections: sections))
    }
}
