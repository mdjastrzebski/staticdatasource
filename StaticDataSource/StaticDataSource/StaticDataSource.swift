//
//  StaticDataSource.swift
//  StaticDataSource
//
//  Created by Maciej Jastrzebski on 22/04/16.
//  Copyright © 2016 Objectivity Ltd. All rights reserved.
//

import Foundation

struct StaticDataSource {
    var sections: [StaticSectionType]

    init(sections: [StaticSectionType]) {
        self.sections = sections
    }

    init(sections: StaticSectionType...) {
        self.init(sections: sections)
    }

    func reuseIdentifierForIndexPath(indexPath: NSIndexPath) -> String? {
        return sections[indexPath.section].reuseIdentifierForItem(indexPath.item)
    }

    func keyForIndexPath(indexPath: NSIndexPath) -> String? {
        return sections[indexPath.section].keyForItem(indexPath.item)
    }
}

//extension StaticDataSource {
//    init(sections: [StaticSectionType?]) {
//        let sections = sections.filter { $0 != nil }.map { $0.optional! }
//        self.init(sections: sections)
//    }
//
//    init(sections: StaticSectionType?...) {
//        self.init(sections: sections)
//    }
//}
