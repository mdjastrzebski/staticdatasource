//
//  ArraySection.swift
//  StaticDataSource
//
//  Created by Maciej Jastrzebski on 22/04/16.
//  Copyright © 2016 Objectivity Ltd. All rights reserved.
//

import Foundation

struct ElementsSection<Element, Cell>: StaticSectionType {
    var header: String?
    var footer: String?
    var reuseIdentifier: String
    var key: String?
    var elements: [Element]

    var itemCount: Int {
        return elements.count
    }

    var bind: (Element, Cell) -> Void

    init?(
        header: String? = nil,
        footer: String? = nil,
        reuseIdentifier: String,
        key: String? = nil,
        elements: [Element],
        bind: (Element, Cell) -> Void)
    {
        guard elements.count > 0 else { return nil }

        self.key = key
        self.header = header
        self.footer = footer
        self.reuseIdentifier = reuseIdentifier
        self.elements = elements
        self.bind = bind
    }

    init?(key: String? = nil, header: String? = nil, footer: String? = nil, elements: [Element], bind: (Element, Cell) -> Void) {
        self.init(header: header, footer: footer, reuseIdentifier: String(Cell), key: key, elements: elements, bind: bind)
    }

    func reuseIdentifierForItem(item: Int) -> String {
        return reuseIdentifier
    }

    func keyForItem(item: Int) -> String? {
        return key
    }

    func bindCell<AnyCell>(cell: AnyCell, withItem item: Int) {
        guard let cell = cell as? Cell else { return }
        let item = elements[item]
        bind(item, cell)
    }
}
