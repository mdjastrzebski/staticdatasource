//
//  StaticDataSource.h
//  StaticDataSource
//
//  Created by Maciej Jastrzebski on 22/04/16.
//  Copyright © 2016 Objectivity Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for StaticDataSource.
FOUNDATION_EXPORT double StaticDataSourceVersionNumber;

//! Project version string for StaticDataSource.
FOUNDATION_EXPORT const unsigned char StaticDataSourceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <StaticDataSource/PublicHeader.h>


