//
//  StaticSectionType.swift
//  StaticDataSource
//
//  Created by Maciej Jastrzebski on 22/04/16.
//  Copyright © 2016 Objectivity Ltd. All rights reserved.
//

import Foundation

protocol StaticSectionType {
    var header: String? { get }
    var footer: String? { get }
    var itemCount: Int { get }

    func reuseIdentifierForItem(item: Int) -> String
    func keyForItem(item: Int) -> String?

    func bindCell<Cell>(cell: Cell, withItem: Int)
}
