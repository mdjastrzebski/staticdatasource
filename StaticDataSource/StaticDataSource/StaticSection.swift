//
//  StaticSection.swift
//  StaticDataSource
//
//  Created by Maciej Jastrzebski on 22/04/16.
//  Copyright © 2016 Objectivity Ltd. All rights reserved.
//

import Foundation

struct StaticSection: StaticSectionType {
    var header: String?
    var footer: String?

    var items: [StaticItemType]
    var itemCount: Int { return items.count }

    init(header: String? = nil, footer: String? = nil, items: [StaticItemType]) {
        self.items = items
        self.header = header
        self.footer = footer
    }

    init(header: String? = nil, footer: String? = nil, items: StaticItemType...) {
        self.init(header: header, footer: footer, items: items)
    }

    func reuseIdentifierForItem(item: Int) -> String {
        return items[item].reuseIdentifier
    }

    func keyForItem(item: Int) -> String? {
        return items[item].key
    }

    func bindCell<Cell>(cell: Cell, withItem item: Int) {
        items[item].bindCell(cell)
    }
}
