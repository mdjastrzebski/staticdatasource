//
//  StaticItemType.swift
//  StaticDataSource
//
//  Created by Maciej Jastrzebski on 22/04/16.
//  Copyright © 2016 Objectivity Ltd. All rights reserved.
//

import Foundation

protocol StaticItemType {
    var reuseIdentifier: String { get }
    var key: String? { get }

    func bindCell<Cell>(cell: Cell)
}
