//
//  StaticItem.swift
//  StaticDataSource
//
//  Created by Maciej Jastrzebski on 22/04/16.
//  Copyright © 2016 Objectivity Ltd. All rights reserved.
//

import Foundation

struct StaticItem<Cell>: StaticItemType {
    var reuseIdentifier: String
    var key: String?

    var bind: (Cell) -> Void

    init(reuseIdentifier: String, key: String? = nil, bind: (Cell) -> Void) {
        self.reuseIdentifier = reuseIdentifier
        self.key = key
        self.bind = bind
    }

    init(key: String? = nil, bind: (Cell) -> Void) {
        self.init(reuseIdentifier: String(Cell), key: key, bind: bind)
    }

    func bindCell<AnyCell>(cell: AnyCell) {
        guard let cell = cell as? Cell else { return }
        bind(cell)
    }
}
